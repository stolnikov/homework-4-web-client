# Bracy docker web server

PSR compatible HTTP server leveraging [stolnikov/bracy package](https://github.com/stolnikov/bracy).

![BracyWeb](https://drive.google.com/uc?export=view&id=1rxE5rvSzWWC51wgDs-Rs7nq7P4JbqQF6)

Self-called anonymous function in index.php acts as entry point. It creates its own scope and keeps the global namespace clean.

# PSR compatibility

**PSR-11 DI container**: [PHP-DI](https://github.com/PHP-DI/PHP-DI).

**PSR-15 middleware dispatcher**: [Relay](https://github.com/relayphp/Relay.Relay) request handler.

**PSR-7 messages**: PSR-15 middleware spec requires implementations to pass along PSR-7 compatible HTTP messages. [Zend Diactoros](https://github.com/zendframework/zend-diactoros/) has PSR-7 implementation of PSR-7 interfaces as well as emitters for flushing PSR-7 responses.

# Project structure
```
├── app
│   ├── bootstrap
│   │   ├── container.php
│   │   └── pipeline.php
│   ├── composer.json
│   ├── public
│   │   └── index.php
│   ├── routes
│   │   └── web.php
│   └── src
│       ├── Handlers
│       │   ├── BracyStatusDispatcher.php
│       │   └── DispatcherInterface.php
│       └── Http
│           └── ActionControllers
│               └── IndexPostHandler.php
├── docker
│   └── nginx
│       ├── conf.d
│       │   └── nginx.conf
│       └── Dockerfile
├── docker-compose.yml
└── README.md
```
# Usage
