<?php

use DI\ContainerBuilder;
use function DI\create;

$containerBuilder = (new ContainerBuilder())
    ->useAutowiring(false)
    ->useAnnotations(false);

$containerBuilder->addDefinitions(
    [
        BracyWeb\Http\ActionControllers\IndexPostHandler::class => create(
            BracyWeb\Http\ActionControllers\IndexPostHandler::class
        ),
    ]
);

return $containerBuilder->build();
