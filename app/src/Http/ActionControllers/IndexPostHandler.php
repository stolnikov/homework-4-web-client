<?php

namespace BracyWeb\Http\ActionControllers;

use Bracy\Validators\BalancedValidator;
use Bracy\Validators\CharsValidator;
use BracyWeb\Handlers\BracyStatusDispatcher;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\TextResponse;

/**
 * Handler of POST requests coming at index page.
 */
class IndexPostHandler implements MiddlewareInterface
{
    /**
     * Process an incoming server request and return a response delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {

        $string = $request->getParsedBody()['string'];

        if (isset($string)) {
            $dispatcher = new BracyStatusDispatcher(
                new BalancedValidator(
                    new CharsValidator()
                )
            );
            list($responseBody, $statusCode) = $dispatcher->process($string);

            return new TextResponse($responseBody, $statusCode);
        }

        return new TextResponse("Parameter 'string' has not been passed.", 400);
    }
}
