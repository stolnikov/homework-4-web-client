<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Self-called anonymous function that creates its own scope
 * and keep the global namespace clean.
 */
(function () {

    /**
     * PSR compatible DI-container
     * @var \Psr\Container\ContainerInterface $container
     */
    $container = require dirname(__DIR__) . '/bootstrap/container.php';

    /**
     * Application web-route
     * @var FastRoute\Dispatcher $routes
     */
    $routes = require dirname(__DIR__) . '/routes/web.php';

    /**
     * Execute programmatic/declarative middleware pipeline and routing
     * configuration statements
     */
    $middlewarePipeline = (require dirname(__DIR__) . '/bootstrap/pipeline.php')(
        $routes,
        $container
    );

    /**
     * Server-side PSR-7 HTTP request
     * @var Psr\Http\Message\ServerRequestInterface $serverRequest
     */
    $serverRequest = Zend\Diactoros\ServerRequestFactory::fromGlobals();

    /**
     * PSR-15 compatible middleware dispatcher (request handler)
     * @var Psr\Http\Server\RequestHandlerInterface $requestHandler
     */
    $requestHandler = new Relay\Relay($middlewarePipeline);
    $response = $requestHandler->handle($serverRequest);

    /**
     * Response emitter for a PHP SAPI environment
     * @var Zend\Diactoros\Response\EmitterInterface $emitter
     */
    $emitter = new Zend\Diactoros\Response\SapiEmitter();
    /**
     * Emit the status line and headers via the header() function, and the
     * body content via the output buffer.
     */
    $emitter->emit($response);
})();
